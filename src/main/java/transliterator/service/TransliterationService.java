package transliterator.service;

/**
 * Created by dsshevchenko.
 *
 * Transliteration service
 */
public interface TransliterationService {

    /**
     * Transliterate text by language to english
     * @param lang language
     * @param text text to be transliterate
     * @return result
     */
    String transliterate(String lang, String text);
}
