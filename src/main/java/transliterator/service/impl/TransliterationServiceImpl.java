package transliterator.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import transliterator.config.RulesConfig;
import transliterator.exception.UnsupportedLanguageException;
import transliterator.service.TransliterationService;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by dsshevchenko.
 */
@Component
public class TransliterationServiceImpl implements TransliterationService {

    @Autowired
    private RulesConfig rulesConfig;

    @Override
    public String transliterate(String lang, String text) {
        if (StringUtils.isEmpty(lang)) {
            throw new IllegalArgumentException("Language should be filled");
        }
        if (StringUtils.isEmpty(text)) {
            throw new IllegalArgumentException("Text should be filled");
        }
        Map<String, String> mapping = rulesConfig.getRules().stream()
                .filter(rule -> rule.getLang().equalsIgnoreCase(lang))
                .map(RulesConfig.Rule::getMapping)
                .findFirst().orElseThrow(() -> new UnsupportedLanguageException("Language is not supported"));
        return Stream.of(text.trim().split(""))
                .map(letter -> mapping.getOrDefault(letter, letter))
                .collect(Collectors.joining());
    }
}
