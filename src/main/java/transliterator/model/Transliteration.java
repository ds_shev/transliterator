package transliterator.model;

/**
 * Created by dsshevchenko.
 */
public class Transliteration {

    private String language;
    private String text;

    public String getLanguage() {
        return language;
    }

    /**
     * Set language property.
     *
     * @param val - new language value.
     * @return the {@link Transliteration} for chaining.
     */
    public Transliteration setLanguage(String val) {
        language = val;
        return this;
    }

    public String getText() {
        return text;
    }

    /**
     * Set text property.
     *
     * @param val - new text value.
     * @return the {@link Transliteration} for chaining.
     */
    public Transliteration setText(String val) {
        text = val;
        return this;
    }
}
