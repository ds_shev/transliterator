package transliterator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import transliterator.exception.UnsupportedLanguageException;
import transliterator.model.Transliteration;
import transliterator.service.TransliterationService;

/**
 * Created by dsshevchenko.
 *
 * Transliteration controller
 */
@RestController
@RequestMapping("/api/v1.0/transliteration")
public class TransliterationController {

    @Autowired
    private TransliterationService transliterationService;

    /**
     * Transliteration words by language
     * @param request provided request
     * @return result
     */
    @RequestMapping(method = RequestMethod.POST)
    public Transliteration transliteration(@RequestBody Transliteration request) {
        String transliteratedText = transliterationService.transliterate(request.getLanguage(), request.getText());
        return new Transliteration().setText(transliteratedText).setLanguage("EN");
    }

    /**
     * Exception handler method. Handle bad requests
     * @param ex handled exception
     * @return response
     */
    @ExceptionHandler({UnsupportedLanguageException.class, IllegalArgumentException.class})
    protected ResponseEntity<String> badRequest(Exception ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
