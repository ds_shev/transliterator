package transliterator.exception;

/**
 * Created by dsshevchenko.
 *
 * Unsupported language exception occurs when rules for provided language is not exist
 */
public class UnsupportedLanguageException extends RuntimeException {

    public UnsupportedLanguageException(String message) {
        super(message);
    }
}
