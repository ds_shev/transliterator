package transliterator.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import transliterator.model.Transliteration;

/**
 * @author dsshevchenko
 * @since <pre>4/4/2018</pre>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TransliterationControllerTests {

    private final String BASE_URL = "/api/v1.0/transliteration";

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldReturnValidTextTransliterationResponse() throws Exception {
        Transliteration request = new Transliteration().setLanguage("RU").setText("клад");
        ResponseEntity<Transliteration> response = restTemplate.postForEntity(BASE_URL, request, Transliteration.class);
        Assert.assertEquals(200, response.getStatusCode().value());
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals("EN", response.getBody().getLanguage());
        Assert.assertEquals("klad", response.getBody().getText());
    }

    @Test
    public void shouldReturnErrorLanguageResponse() throws Exception {
        Transliteration request = new Transliteration().setLanguage("US").setText("клад");
        ResponseEntity<String> response = restTemplate.postForEntity(BASE_URL, request, String.class);
        Assert.assertEquals(400, response.getStatusCode().value());
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals("Language is not supported", response.getBody());
    }

    @Test
    public void shouldReturnEmptyLanguageBadRequest() throws Exception {
        Transliteration request = new Transliteration().setText("клад");
        ResponseEntity<String> response = restTemplate.postForEntity(BASE_URL, request, String.class);
        Assert.assertEquals(400, response.getStatusCode().value());
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals("Language should be filled", response.getBody());
    }

    @Test
    public void shouldReturnEmptyTextBadRequest() throws Exception {
        Transliteration request = new Transliteration().setLanguage("RU").setText("");
        ResponseEntity<String> response = restTemplate.postForEntity(BASE_URL, request, String.class);
        Assert.assertEquals(400, response.getStatusCode().value());
        Assert.assertEquals("Text should be filled", response.getBody());
    }
}
