package transliterator.service;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import transliterator.exception.UnsupportedLanguageException;

/**
 * Created by dsshevchenko.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TransliterationServiceTests {

    @Autowired
    private TransliterationService transliterationService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldTransliterateRussianText() throws Exception {
        String result = transliterationService.transliterate("RU", "транслитерация русских слов");
        Assert.assertEquals("transliteracziya russkix slov", result);
    }

    @Test
    public void shouldTransliterateRussianTextWithCase() throws Exception {
        String result = transliterationService.transliterate("RU", "трансЛИТЕРация Русских СЛОВ");
        Assert.assertEquals("transЛИТЕРacziya Рusskix СЛОВ", result);
    }

    @Test
    public void shouldTransliterateUkraineText() throws Exception {
        String result = transliterationService.transliterate("UA", "транслітерація українських слів");
        Assert.assertEquals("transliteratsiia ukrayi*nskykh sliv", result);
    }

    @Test
    public void shouldTransliterateUkraineTextWithCase() throws Exception {
        String result = transliterationService.transliterate("UA", "ТранслітеРАЦІЯ Українських СЛІВ");
        Assert.assertEquals("ТransliteРАЦІЯ Уkrayi*nskykh СЛІВ", result);
    }

    @Test
    public void shouldTransliterateTextWithAbsentLang() throws Exception {
        thrown.expect(UnsupportedLanguageException.class);
        thrown.expectMessage("Language is not supported");
        transliterationService.transliterate("BLR", "транслітарацыя беларускіх словаў");
    }
}
